extern crate markov;
extern crate rustc_serialize;

use markov::generate_word;

use std::fs::File;
use std::io::Read;
use std::process::exit;

use std::collections::{HashMap, BTreeMap};

use rustc_serialize::json;

fn main() {
    let file_name = "sequence.json";

    let mut word = String::new();
    loop {
        let contents = get_file_contents(&file_name);
        let sequence = create_sequence_from_json_string(&contents);
        word = generate_word(&sequence).unwrap_or("fail".into());

        if word.len() > 3{
            break;
        }
    }
    println!("{}", word);
}

fn get_file_contents(file_name: &str) -> String {
    let mut file = match File::open(file_name) {
        Ok(file) => file,
        Err(e) => {
            println!("Failed to open file: {:?}", e);
            exit(-1);
        }
    };

    let mut contents = String::new();
    match file.read_to_string(&mut contents) {
        Ok(_) => {}
        Err(e) => {
            println!("Failed to read {}, {:?}", file_name, e);
            exit(-1);
        }
    }
    return contents;
}

fn create_sequence_from_json_string(string: &String) -> HashMap<char, HashMap<char, f32>> {
    let json = match json::Json::from_str(string) {
        Ok(json) => json,
        Err(e) => {
            println!("Failed to parse sequence json: {:?}", e);
            exit(-1);
        }
    };

    let mut sequence: HashMap<char, HashMap<char, f32>> = HashMap::new();
    let json: &BTreeMap<String, _> = json.as_object().unwrap();

    for (key, value) in json {
        if key.len() != 1 {
            println!("Error {} is not a char", key);
            exit(-1);
        }
        let character: char = key.chars().nth(0).unwrap();
        let map = btree_to_hash_map(value.as_object().unwrap());
        sequence.insert(character, map);
    }
    return sequence;
}

fn btree_to_hash_map(tree: &BTreeMap<String, json::Json>) -> HashMap<char, f32> {
    let mut map = HashMap::new();
    for (key, value) in tree {
        map.insert(key.chars().nth(0).unwrap(), value.as_f64().unwrap() as f32);
    }
    return map;
}
