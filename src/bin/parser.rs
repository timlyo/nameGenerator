extern crate rustc_serialize;
extern crate num;

extern crate markov;

use markov::parse_words;

use std::collections::HashMap;
use std::fs::File;

use std::io::{Read, Write};
use std::process::exit;
use rustc_serialize::json;


fn main() {
    let file_name = "data/male_names.csv";

    let words = get_variables_from_file(&file_name);
    let sequence = parse_words(&words);
    save_to_file("sequence.json", &sequence);
}

fn get_variables_from_file(file_name: &str) -> Vec<String> {
    println!("Parsing {}", file_name);
    let mut file = match File::open(file_name) {
        Ok(file) => file,
        Err(e) => {
            println!("Failed to open file: {}", e);
            exit(-1);
        }
    };

    let mut content = String::new();
    file.read_to_string(&mut content).unwrap();

    let variables: Vec<String> = content.split(",")
                                        .map(|name| str::replace(name.trim(), "\n", "") + ".")
                                        .collect();
    return variables;
}

fn save_to_file(name: &str, sequence: &HashMap<char, HashMap<char, f32>>){
    let json = match json::encode(&sequence) {
        Ok(json) => json,
        Err(e) => {
            println!("{:?}", e);
            exit(-1);
        }

    };

    let mut output_file = match File::create(name) {
        Ok(file) => file,
        Err(e) => {
            println!("Error opening output file: {:?}", e);
            exit(-1);
        }
    };

    match output_file.write_all(json.as_bytes()) {
        Ok(_) => {}
        Err(e) => {
            println!("Error writing to output file {:?}", e);
            exit(-1);
        }
    }
}
