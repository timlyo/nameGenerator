#![feature(iter_arith)]

extern crate rustc_serialize;
extern crate num;
extern crate rand;

use std::collections::HashMap;
use std::io::Read;
use rand::{Rng, StdRng};
use rand::distributions::{Range, Sample};

pub fn generate_word(sequence: &HashMap<char, HashMap<char, f32>>) -> Option<String> {
    let mut rng = match StdRng::new(){
        Ok(rng) => rng,
        Err(e) => {
            println!("Failed to create rng: {:?}", e);
            return None;
        }
    };
    return Some(generate_word_from_seed(sequence, &mut rng));
}

pub fn generate_word_from_seed<R: Rng>(sequence: &HashMap<char, HashMap<char, f32>>,
                                       rng: &mut R)
                                       -> String {
    let mut word: String = String::new();

    let mut key = select_random_key(&sequence.keys().collect(), rng);
    loop{
        let values = sequence.get(&key);
        word.push(key);
        key = select_from_values(&sequence.get(&key).unwrap(), rng);
        if key == '.'{
            break;
        }
    }

    return word;
}

/// Generate markov chain information from a vector of strings
pub fn parse_words(words: &Vec<String>) -> HashMap<char, HashMap<char, f32>> {
    let mut table: HashMap<char, Vec<char>> = HashMap::new();
    for word in words {
        let characters: Vec<char> = word.chars().collect();
        for group in characters.windows(2) {
            let current = group[0];
            let next = group[1];

            // create key if it doesn't exist
            if !table.contains_key(&current) {
                table.insert(current, Vec::new());
            }

            // add next letter
            table.get_mut(&current).unwrap().push(next);
        }
    }

    let mut model: HashMap<char, HashMap<char, f32>> = HashMap::new();
    for (character, paths) in table {
        let char_counts: HashMap<char, u32> = count_elements_in_vec(&paths);
        let probabilities = get_probabilities_from_map(&char_counts);
        model.insert(character, probabilities);
    }

    return model;
}

fn count_elements_in_vec(vector: &Vec<char>) -> HashMap<char, u32> {
    let mut result: HashMap<char, u32> = HashMap::new();

    for element in vector {
        if !result.contains_key(&element) {
            result.insert(element.clone(), 1);
        } else {
            *result.get_mut(&element).unwrap() += 1;
        }
    }

    return result;
}

fn get_probabilities_from_map(map: &HashMap<char, u32>) -> HashMap<char, f32> {
    let mut result: HashMap<char, f32> = HashMap::new();

    let total: u32 = map.values().sum();

    for (key, value) in map {
        let probability: f32 = (*value as f32) / (total as f32);
        result.insert(*key, probability);
    }

    return result;
}

fn select_random_key<R: Rng>(keys: &Vec<&char>, rng: &mut R) -> char {
    let mut range = Range::new(0, keys.len());
    let index = range.sample(rng);
    return keys[index].clone();
}

fn select_from_values<R: Rng>(keys: &HashMap<char, f32>, rng: &mut R) -> char{
    let rand_num:f32 = rng.gen();

    let mut probability_sum = 0.0;
    for (key, probability) in keys{
        probability_sum += *probability;
        if(rand_num < probability_sum){
            return key.clone();
        }
    }

    return '.';
}

#[cfg(test)]
mod tests {
    use std::collections::HashMap;

    #[test]
    fn test_get_probabilities_from_map() {
        let mut map: HashMap<char, u32> = HashMap::new();
        map.insert('a', 1);
        map.insert('b', 1);
        map.insert('c', 1);
        map.insert('d', 1);

        let result = super::get_probabilities_from_map(&map);
        assert_eq!(*result.get(&'a').unwrap(), 0.25);
        assert_eq!(*result.get(&'b').unwrap(), 0.25);
        assert_eq!(*result.get(&'c').unwrap(), 0.25);
        assert_eq!(*result.get(&'d').unwrap(), 0.25);
    }

    #[test]
    fn test_get_probabilities_from_map_2() {
        let mut map: HashMap<char, u32> = HashMap::new();
        map.insert('a', 1);
        map.insert('b', 3);

        let result = super::get_probabilities_from_map(&map);
        assert_eq!(*result.get(&'a').unwrap(), 0.25);
        assert_eq!(*result.get(&'b').unwrap(), 0.75);
    }

    #[test]
    fn test_count_elements_in_vec() {
        let vector = vec!['a', 'b', 'c', 'a'];
        let result = super::count_elements_in_vec(&vector);

        assert_eq!(*result.get(&'a').unwrap(), 2);
        assert_eq!(*result.get(&'b').unwrap(), 1);
        assert_eq!(*result.get(&'b').unwrap(), 1);
    }
}
